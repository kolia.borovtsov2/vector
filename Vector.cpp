#include <iostream>
#include "Vector.h"
#include <stdexcept>

Vector::Vector(const Value* rawArray, const size_t size, float coef)
{	
	Vector::_size = size;
	Vector::_capacity = size;
	Vector::_multiplicativeCoef = coef;
	Vector::_data = new Value[_capacity];

	for(size_t i = 0; i < _size; i++)
	{
		_data[i] = rawArray[i];
	}
}

Vector::Vector(const Vector& other)
{
	Vector::_size = other.size();
	Vector::_capacity = other.size();
	Vector::_multiplicativeCoef = other._multiplicativeCoef;
	Vector::_data = new Value[_capacity];

	for (int i = 0; i < _capacity; i++)
	{
		_data[i] = other._data[i];
	}
}

Vector& Vector::operator=(const Vector& other)
{
	if (this == &other)
	{
		return *this;	
	}
	
	Vector::_size = other.size();
	Vector::_capacity = other.capacity();
	Vector::_multiplicativeCoef = other._multiplicativeCoef;
	Vector::_data = new Value[_size];
	
	for (size_t i = 0; i < _size; i++)
	{
		_data[i] = other._data[i];
	}
	
	return *this;
}

Vector::Vector(Vector&& other) noexcept
{ 
	Vector::_size = other._size;
	Vector::_capacity = other.capacity();
	Vector::_multiplicativeCoef = other._multiplicativeCoef;
	Vector::_data = other._data;

	other._data = nullptr;
	other._size = 0;
	other._capacity = 0;
	other._multiplicativeCoef = 2.0f;
}

Vector& Vector::operator=(Vector&& other) noexcept
{
	if (this == &other)
	{
		return *this;
	}

	Vector::_size = other._size;
	Vector::_capacity = other.capacity();
	Vector::_multiplicativeCoef = other._multiplicativeCoef;
	Vector::_data = other._data;
		
	other._data = nullptr;
	other._size = 0;
	other._capacity = 0;
	other._multiplicativeCoef = 2.0f;
	
	return *this;
}

Vector::~Vector()
{
	delete[] _data;
	_size = 0;
	_capacity = 0;
	_multiplicativeCoef = 2.0f;
}

void Vector::memory()
{
	_capacity *= _multiplicativeCoef;
    Value* tmp = new Value[_size];
	
	for (size_t i = 0; i < _size; i++)
	{
		tmp[i] = _data[i];
    }
		
    delete[] _data;
	_data = new Value[_capacity];
	
	for (int i = 0; i < _size; i++)
	{
		_data[i] = tmp[i];
	}
	
	delete[] tmp;
}

void Vector::pushBack(const Value& value)
{
	if (_size == _capacity)
	{
		_capacity += 1;
		memory();
    }

   	_data[_size] = value;
	_size += 1;
}

void Vector::pushFront(const Value& value)
{
    if (_size == _capacity)
	{
		_capacity += 1;
        memory();
    }
	
    for (size_t i = _size+1; i > 0 ; i--)
	{
        _data[i] = _data[i - 1];
    }
	
    _data[0] = value;
    _size += 1;
}

void Vector::insert(const Value& value, size_t pos)
{
	if (_size < pos)
	{
        throw std::invalid_argument("ERROR");
    }
	
    if (_size == _capacity)
	{
        memory();
    }
	
    for (size_t i = _size+1; i > pos; i--)
	{
        _data[i] = _data[i - 1];
    }

    _data[pos] = value;
    _size++;
}

void Vector::insert(const Value* values, size_t size ,size_t pos)
{
	if (_size < pos)
	{
    	throw std::invalid_argument("ERROR");
    }
	
	_size += size;
	
    while (_size >= _capacity)
    {
		memory();
    }
	
    size_t counter = 0;
	
	while (counter != size)
	{
		for (size_t i = _size - 1; i > pos; i--)
    	{
        	_data[i] = _data[i - 1];
    	}
		counter++;
	}
   
    for (size_t i = 0; i < size; i++)
    {
    	_data[pos + i] = values[i];
    }
}

void Vector::insert(const Vector& vector, size_t pos)
{
	if (_size < pos)
	{
    	throw std::invalid_argument("ERROR");
    }
	
    insert(vector._data, vector._size, pos);
}

void Vector::popBack()
{
    if (_size > 0) 
	{
		_data[_size-1] = 0;
    	_size--;
    }
		
    else
	{
    	throw std::out_of_range("Vector is empty");
    }
}

void Vector::popFront()
{
    if (_size > 0)
	{
    	for (size_t i = 0; i < _size; i++)
		{
    	    _data[i] = _data[i + 1];
		}
		
    	_size--;
    }
		
    else
	{
    	throw std::out_of_range("Vector is empty");
    }
}

void Vector::erase(size_t pos, size_t count)
{
	if (count + pos > _size)
    {
        _size = _size - (_size - pos);
    }
		
	else
	{
        for (size_t i = 0; i < _size - count + 1; i++)
        {
            _data[i + pos] = _data[i + pos + count];
        }
		
	_size -= count;
    }
}

void Vector::eraseBetween(size_t beginPos, size_t endPos)
{
    erase(beginPos, endPos - beginPos);
}

size_t Vector::size() const
{
    return _size;
}

size_t Vector::capacity() const
{
    return _capacity;
}

double Vector::loadFactor() const
{
    return double(_size) / _capacity;
}

Value& Vector::operator[](size_t idx)
{
	if (_size < idx)
	{
        throw std::invalid_argument("ERROR");
    }

    return _data[idx];
}

const Value& Vector::operator[](size_t idx) const
{
	if (_size < idx)
	{
        throw std::invalid_argument("ERROR");
    }

    return _data[idx];
}

long long Vector::find(const Value& value) const
{
    for(size_t i = 0; i < _size; i++)
	{
    	if (_data[i] == value)
		{
    	    return i;
    	}
    }

    return -1;
}

void Vector::reserve(size_t capacity)
{
    if (capacity <= _capacity)
	{
        return;
    }

	_capacity = capacity;

    Value* tmp = new Value[capacity];
	
    for (size_t i = 0; i < _size; i++)
	{
        tmp[i] = _data[i];
    }
    delete[] _data;
    _data = tmp;
	
	 
}

void Vector::shrinkToFit()
{
	_capacity = _size;
    Value* tmp = new Value[_capacity];
	
	for (size_t i = 0; i < _capacity; i++)
	{
		tmp[i] = _data[i];
    }

    delete[] _data;
	
	_data = tmp;
}
Vector::Iterator::Iterator(Value* ptr)
{
       _ptr = ptr;
}

Value& Vector::Iterator::operator*()
{
    return *this->_ptr;
}

const Value& Vector::Iterator::operator*() const
{
    return *this->_ptr;
}

Vector::Iterator Vector::Iterator::operator++()
{
    _ptr++;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int)
{
	Value* n = _ptr;
    _ptr++;
    return Iterator(n);
}

Value* Vector::Iterator::operator->()
{
    return _ptr;
}

const Value* Vector::Iterator::operator->() const
{
    return _ptr;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const
{
    return _ptr == other._ptr;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const
{
    return _ptr != other._ptr;
}

Vector::Iterator Vector::begin()
{
    Vector::Iterator tmp(_data);
    return tmp;
}

Vector::Iterator Vector::end()
{
    Vector::Iterator tmp(_data + _size);
    return tmp;
}
